import Foundation
import XCTest

extension XCTestCase {
    /// Asserts that the first value matches the regular expression defined by the second expression.
    /// - Parameters:
    ///   - expression1: An expression of type T, where T conforms to StringProtocol.
    ///   - expression2: A second expression of type T, defining a regular expression.
    ///   - message: An optional description of a failure.
    ///   - file: The file where the failure occurs.
    ///           The default is the filename of the test case where you call this function.
    ///   - line: The line number where the failure occurs. The default is the line number where you call this function.
    public func assertMatches<T>(
        _ expression1: @autoclosure () throws -> T,
        _ expression2: @autoclosure () throws -> T,
        _ message: @autoclosure () -> String = "",
        file: StaticString = #filePath,
        line: UInt = #line
    ) where T: StringProtocol {
        do {
            let text = try expression1()
            let regex = try expression2()
            let matches = text.range(of: regex, options: .regularExpression)
            if matches == nil {
                XCTFail(
                    "\(String(text).debugDescription) does not match \(String(regex).debugDescription)",
                    file: file,
                    line: line
                )
            }
        } catch {
            XCTFail("invalid expression; only Strings are acceptable", file: file, line: line)
        }
    }
}

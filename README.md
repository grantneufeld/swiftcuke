# swiftcuke

Process Gherkin format feature files for Swift language projects.

## Legal

Copyright ©2021 Grant Neufeld.

Available for use under the terms of the [Do No Harm License](license.md).

Participants in, and contributors to, this project are subject to the [Code of Conduct](code_of_conduct.md).
